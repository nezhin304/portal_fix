const NpddModel = require("../models/npdd");
const UserModel = require("../models/user");
const EquipmentModel = require("../models/equipment");

module.exports = {
  getNpdds(req, res, next) {
    NpddModel.find()
      .select(
        "_id number description wastedTime dateStart dateEnd isDone files"
      )
      .sort({ dateStart: -1 })
      .populate("equipments", "_id name")
      .populate("users", "_id login username")
      .then((npdds) => {
        res.json(npdds);
      })
      .catch((err) => {
        next(err);
      });
  },

  async addNpdd(req, res, next) {
    const equipmentsIds = await EquipmentModel.find({
      name: {
        $in: req.body.equipments.map((eq) => {
          return eq.name;
        }),
      },
    }).then((equipments) => {
      const eqIds = equipments.map((eq) => {
        return eq._id;
      });
      return eqIds;
    });

    const newNpdd = new NpddModel({
      number: req.body.number,
      description: req.body.description,
      wastedTime: req.body.wastedTime,
      dateStart: req.body.dateStart,
      dateEnd: req.body.dateEnd,
      isDone: req.body.isDone,
      equipments: equipmentsIds,
      users: req.user._id,
    });
    newNpdd
      .save()
      .then((npdd) => {
        res.status(201).json(npdd);
      })
      .catch((err) => {
        next(err);
      });
  },

  modNpdd(req, res, next) {},

  getNpddByNumberOrDescription(req, res, next) {
    const regex = new RegExp(req.query.search);
    NpddModel.find({
      $or: [
        { number: { $regex: regex, $options: "i" } },
        { description: { $regex: regex, $options: "i" } },
      ],
    })
      .select(
        "_id number description wastedTime dateStart dateEnd isDone files"
      )
      .populate("equipments", "_id name")
      .populate("users", "_id login username")
      .then((npdds) => {
        res.json(npdds);
      })
      .catch((err) => {
        next(err);
      });
  },
};
