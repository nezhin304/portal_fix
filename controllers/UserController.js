const UserModel = require("../models/user");
const utils = require("../lib/utils");

module.exports = {
  getUsers(req, res, next) {
    UserModel.find()
      .select("_id login")
      .then((users) => {
        res.json(users);
      })
      .catch((err) => {
        next(err);
      });
  },

  registerUser(req, res, next) {
    const saltHash = utils.genPassword(req.body.password);
    const salt = saltHash.salt;
    const hash = saltHash.hash;

    const newUser = new UserModel({
      login: req.body.login,
      hash: hash,
      salt: salt,
    });

    newUser
      .save()
      .then((user) => {
        const jwt = utils.issueJWT(user);
        const userDto = { id: user._id, login: user.login };
        res.status(201).json({
          success: true,
          user: userDto,
          token: jwt.token,
          expiresIn: jwt.expires,
        });
      })
      .catch((err) => {
        res.status(400).json({
          success: false,
          message: err.message,
        });
      });
  },

  loginUser(req, res, next) {
    // console.log(req.headers);
    UserModel.findOne({ login: req.body.login })
      .then((user) => {
        if (!user) {
          res.status(401).json({
            success: false,
            message: "Incorrect login or password",
          });
        }
        const isValid = utils.validPassword(
          req.body.password,
          user.hash,
          user.salt
        );
        if (isValid) {
          const tokenObject = utils.issueJWT(user);
          const userDto = {
            _id: user._id,
            login: user.login,
          };
          res.status(200).json({
            success: true,
            user: userDto,
            token: tokenObject.token,
            expiresIn: tokenObject.expires,
          });
        } else {
          res.status(401).json({
            success: false,
            message: "Incorrect login or password",
          });
        }
      })
      .catch((err) => {
        next(err);
      });
  },
};
