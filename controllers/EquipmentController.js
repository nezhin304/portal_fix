const EquipmentModel = require("../models/equipment");

module.exports = {
  getEquipments(req, res, next) {
    EquipmentModel.find()
      .then((equipments) => {
        const equipmentsDto = equipments.map((equipment) => {
          return {
            id: equipment._id,
            name: equipment.name,
            description: equipment.description,
          };
        });
        res.json({ equipments: equipmentsDto });
      })
      .catch((err) => {
        next(err);
      });
  },

  addEquipment(req, res, next) {
    const newEquipment = new EquipmentModel({
      name: req.body.name,
      description: req.body.description,
    });

    newEquipment
      .save()
      .then((equipment) => {
        const equipmentDto = {
          id: equipment._id,
          name: equipment.name,
          description: equipment.description,
        };
        res.status(201).json({ success: true, equipment: equipmentDto });
      })
      .catch((err) => {
        next(err);
      });
  },
  modEquipment(req, res, next) {},
};
