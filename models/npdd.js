const mongoose = require("mongoose");
const uniqueValidator = require("mongoose-unique-validator");

const { Schema } = mongoose;

const npddSchema = new Schema({
  number: {
    type: Schema.Types.String,
    required: true,
    unique: true,
  },
  description: {
    type: Schema.Types.String,
    required: true,
  },
  wastedTime: {
    type: Schema.Types.Number,
  },
  dateStart: {
    type: Schema.Types.Date,
    required: true,
  },
  dateEnd: {
    type: Schema.Types.Date,
  },
  isDone: {
    type: Schema.Types.Boolean,
    default: false,
  },
  files: [
    {
      type: Schema.Types.String,
    },
  ],
  equipments: [
    {
      type: Schema.Types.ObjectId,
      ref: "Equipment",
    },
  ],
  users: [
    {
      type: Schema.Types.ObjectId,
      ref: "User",
    },
  ],
});

npddSchema.plugin(uniqueValidator);
const model = mongoose.model("Npdd", npddSchema);
module.exports = model;
