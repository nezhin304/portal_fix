const mongoose = require("mongoose");
const uniqueValidator = require("mongoose-unique-validator");
require("mongoose-type-email");

const { Schema } = mongoose;

const userSchema = new Schema({
  login: {
    type: Schema.Types.String,
    required: true,
    maxlength: 32,
    unique: true,
  },
  username: {
    type: Schema.Types.String,
    maxlength: 32,
    // unique: true,
  },
  email: {
    type: mongoose.SchemaTypes.Email,
    maxlength: 32,
    // unique: true,
  },
  hash: {
    type: Schema.Types.String,
    require: true,
    unique: true,
  },
  salt: {
    type: Schema.Types.String,
    require: true,
    unique: true,
  },
});

userSchema.plugin(uniqueValidator);
const model = mongoose.model("User", userSchema);
module.exports = model;
