const mongoose = require("mongoose");
const uniqueValidator = require("mongoose-unique-validator");

const { Schema } = mongoose;

const equipmentSchema = new Schema({
  name: {
    type: Schema.Types.String,
    required: true,
    unique: true,
  },
  description: {
    type: Schema.Types.String,
    // required: true,
  },
});

equipmentSchema.plugin(uniqueValidator);
const model = mongoose.model("Equipment", equipmentSchema);
module.exports = model;
