const express = require("express");
const router = express.Router();
const passport = require("passport");
const UserController = require("../controllers/UserController");
const EquipmentController = require("../controllers/EquipmentController");
const NpddController = require("../controllers/NpddController");
const IndexController = require("../controllers/IndexContreller");

module.exports = function (app) {
  // index route
  router.route("/").get((...params) => IndexController.getIndex(...params)),
    // user routes
    router
      .route("/users") // get all users
      .get(passport.authenticate("jwt", { session: false }), (...params) =>
        UserController.getUsers(...params)
      );

  router
    .route("/users/register") // get all users
    .post((...params) => UserController.registerUser(...params));

  router
    .route("/users/login") // get all users
    .post((...params) => UserController.loginUser(...params));

  // equipments routes
  router
    .route("/equipments")
    .get(passport.authenticate("jwt", { session: false }), (...params) =>
      EquipmentController.getEquipments(...params)
    );

  router
    .route("/equipments/add")
    .post(passport.authenticate("jwt", { session: false }), (...params) =>
      EquipmentController.addEquipment(...params)
    );

  // NPDD routes
  router
    .route("/npdds")
    .get(passport.authenticate("jwt", { session: false }), (...params) =>
      NpddController.getNpdds(...params)
    );

  router
    .route("/npdds/add")
    .post(passport.authenticate("jwt", { session: false }), (...params) =>
      NpddController.addNpdd(...params)
    );

  router
    .route("/npdds/")
    .post(passport.authenticate("jwt", { session: false }), (...params) =>
      NpddController.getNpdds(...params)
    );

    router
    .route("/npdds/search")
    .get(passport.authenticate("jwt", { session: false }), (...params) =>
      NpddController.getNpddByNumberOrDescription(...params)
    );

  app.use("/", router);
};
