var createError = require("http-errors");
var express = require("express");
var cors = require('cors');
var path = require("path");
var logger = require("morgan");
var sassMiddleware = require("node-sass-middleware");
const passport = require("passport");

var app = express();

app.use(logger("dev"));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(
  sassMiddleware({
    src: path.join(__dirname, "public"),
    dest: path.join(__dirname, "public"),
    indentedSyntax: true, // true = .sass and false = .scss
    sourceMap: true,
  })
);
app.use(express.static(path.join(__dirname, "public")));
app.use(cors());

// Configures the database and opens a global connection that can be used in any module with `mongoose.connection`
require("./config/database");

// Pass the global passport object into the configuration function
require("./config/passport")(passport);

require("./routes/routes")(app);

// catch 404 and forward to error handler
app.use(function (req, res, next) {
  next(createError(404));
});

// error handler
app.use(function (err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get("env") === "development" ? err : {};

  // render the error page
  res.status(err.status || 500).json({ err: err });
});

module.exports = app;
